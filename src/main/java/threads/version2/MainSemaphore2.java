package threads.version2;
import threads.Foo;
import java.util.concurrent.Semaphore;

public class MainSemaphore2 {
    public static void main(String[] args) {
        Foo f = new Foo();
        Printer printer = new Printer();

        Semaphore semaphore = new Semaphore(1);
        
        class Thread1 implements Runnable{
            private Printer printer;
            public Thread1(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                printer.printFirst();
                semaphore.release();
            }
        }

        class Thread2 implements Runnable{
            private Printer printer;
            public Thread2(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                printer.printSecond();
                semaphore.release();
            }
        }

        class Thread3 implements Runnable{
            private Printer printer;
            public Thread3(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                printer.printThird();
                semaphore.release();
            }
        }

        f.first(new Thread1(printer));
        f.second(new Thread2(printer));
        f.third(new Thread3(printer));
    }
}
