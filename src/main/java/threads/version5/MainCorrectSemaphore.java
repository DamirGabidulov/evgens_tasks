package threads.version5;

import java.util.concurrent.ExecutionException;

public class MainCorrectSemaphore {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Foo1 foo = new Foo1();
        Thread1 thread1 = new Thread1(foo);
        Thread2 thread2 = new Thread2(foo);
        Thread3 thread3 = new Thread3(foo);


        new Thread(thread2).start();
        new Thread(thread3).start();
        new Thread(thread1).start();

    }
}
