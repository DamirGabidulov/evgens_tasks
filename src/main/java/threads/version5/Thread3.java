package threads.version5;

public class Thread3 implements Runnable{
    private Foo1 foo1;
    public Thread3(Foo1 foo1) {
        this.foo1 = foo1;
    }
    @Override
    public void run() {
        foo1.third();
    }
}