package threads.version5;

import threads.version2.Printer;

public class Thread1 implements Runnable{
    private Foo1 foo1;
    public Thread1(Foo1 foo1) {
        this.foo1 = foo1;
    }
    @Override
    public void run() {
        foo1.first();
    }
}