package threads.version5;

import threads.version2.Printer;

public class Thread2 implements Runnable{
    private Foo1 foo1;
    public Thread2(Foo1 foo1) {
        this.foo1 = foo1;
    }
    @Override
    public void run() {
        foo1.second();
    }
}