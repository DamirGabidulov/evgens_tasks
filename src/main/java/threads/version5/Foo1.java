package threads.version5;

import java.util.concurrent.Semaphore;

public class Foo1 {
    private Semaphore semaphore1 = new Semaphore(0);
    private Semaphore semaphore2 = new Semaphore(0);
    private Semaphore semaphore3 = new Semaphore(1);

    public void first() {
        try {
            semaphore3.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("first");
        semaphore2.release();
    }

    public void second() {
        try {
            semaphore2.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("second");
        semaphore1.release();
    }

    public void third() {
        try {
            semaphore1.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.println("third");
        semaphore2.release();
    }
}
