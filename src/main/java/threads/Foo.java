package threads;

public class Foo {
    public void first(Runnable r) {
        Thread newThread = new Thread(r);
        newThread.start();
    }

    public void second(Runnable r) {
        Thread newThread = new Thread(r);
        newThread.start();
    }

    public void third(Runnable r) {
        Thread newThread = new Thread(r);
        newThread.start();
    }
}
