package threads.version1;

import threads.Foo;

import java.util.concurrent.Semaphore;

public class MainSemaphore {
    // этот метод работы с симафором не работает (иногда)
    public static void main(String[] args) {
        Foo f = new Foo();

        Semaphore semaphore = new Semaphore(1);

        f.first(() -> {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.print("first");
            semaphore.release();
        });

        f.second(() -> {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.print("second");
            semaphore.release();
        });

        f.third(() -> {
            try {
                semaphore.acquire();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.print("third");
            semaphore.release();
        });
    }
}
