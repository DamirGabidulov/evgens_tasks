package threads.version4;
import threads.Foo;

import java.util.concurrent.locks.ReentrantLock;

public class MainReentrantLock {
    public static void main(String[] args) {
        Foo f = new Foo();
        Printer printer = new Printer();
        ReentrantLock lock = new ReentrantLock();

        class Thread1 implements Runnable{
            private Printer printer;
            public Thread1(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                lock.lock();
                printer.printFirst();
                lock.unlock();
            }
        }

        class Thread2 implements Runnable{
            private Printer printer;
            public Thread2(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                try {
                    //во втором и третьем потоке специально разный шаг sleep выбран, так как по сути вызываются все эти потоки одновременно
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                lock.lock();
                printer.printSecond();
                lock.unlock();
            }
        }

        class Thread3 implements Runnable{
            private Printer printer;
            public Thread3(Printer printer) {
                this.printer = printer;
            }
            @Override
            public void run() {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                lock.lock();
                printer.printThird();
                lock.unlock();
            }
        }

        f.first(new Thread1(printer));
        f.second(new Thread2(printer));
        f.third(new Thread3(printer));
    }
}
