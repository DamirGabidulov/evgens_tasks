package threads.version6;

import java.util.concurrent.*;

public class MainSemaphoreWithCompletableFuture {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        Foo1 foo = new Foo1();

        CompletableFuture<Void> future1 = CompletableFuture.runAsync(() -> foo.first());
        CompletableFuture<Void> future2 = CompletableFuture.runAsync(() -> foo.second());
        CompletableFuture<Void> future3 = CompletableFuture.runAsync(() -> foo.third());

        future3.get();
        future2.get();
        future1.get();

    }
}
