package threads.version6;

import java.util.concurrent.Semaphore;

public class Foo1 {
    private Semaphore semaphore1 = new Semaphore(0);
    private Semaphore semaphore2 = new Semaphore(0);
    private Semaphore semaphore3 = new Semaphore(1);

    public void first() {
        try {
            semaphore3.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.print("First");
        semaphore2.release();
    }

    public void second() {
        try {
            semaphore2.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.print("Second");
        semaphore1.release();
    }

    public void third() {
        try {
            semaphore1.acquire();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        System.out.print("Third");
        semaphore2.release();
    }
}
