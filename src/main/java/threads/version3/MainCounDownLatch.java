package threads.version3;
import threads.Foo;
import java.util.concurrent.CountDownLatch;

public class MainCounDownLatch {
    public static void main(String[] args) {

        Foo foo = new Foo();
        CountDownLatch firstCountDownLatch = new CountDownLatch(1);
        CountDownLatch secondCountDownLatch = new CountDownLatch(1);
        
        foo.first(() -> {
            System.out.print("first");
            firstCountDownLatch.countDown();
        });

        foo.second(() -> {
            try {
                firstCountDownLatch.await();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
            System.out.print("second");
            secondCountDownLatch.countDown();
        });

        foo.third(() -> {
            try {
                secondCountDownLatch.await();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException(e);
            }
            System.out.print("third");
        });
    }
}
