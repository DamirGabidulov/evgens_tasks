package patterns.creational.builder;

public class Worker {
    private String name;
    private int rang;
    private Specialization specialization;


    public Worker(String name, int rang, Specialization specialization) {
        this.name = name;
        this.rang = rang;
        this.specialization = specialization;
    }


    public String getName() {
        return this.name;
    }

    public int getRang() {
        return this.rang;
    }

    public Specialization getSpecialization() {
        return this.specialization;
    }


    @Override
    public String toString() {
        return "{" +
            " name='" + getName() + "'" +
            ", rang='" + getRang() + "'" +
            ", specialization='" + getSpecialization() + "'" +
            "}";
    }

}
