package patterns.creational.builder;

public class Director {
    public void constructTopWorker(Builder builder){
        builder.setName("Ivan");
        builder.setRang(100);
        builder.setSpecialization();
    }

    public void constructYoungWorker(Builder builder){
        builder.setName("Artem");
        builder.setRang(2);
        builder.setSpecialization();
    }
}
