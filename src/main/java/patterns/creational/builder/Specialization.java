package patterns.creational.builder;

public enum Specialization {
    WOOD, BRICK;
}
