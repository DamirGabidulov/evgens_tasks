package patterns.creational.builder;

public interface Builder {
    void setName(String name);
    void setRang(int rang);
    void setSpecialization();
    Worker getWorker();
}
