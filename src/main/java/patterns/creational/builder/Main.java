package patterns.creational.builder;

public class Main {
    public static void main(String[] args) {
        Builder builder = new WoodWorkerBuilder();
        Director director = new Director();
        director.constructYoungWorker(builder);
        Worker worker = builder.getWorker();
        System.out.println(worker);

    }
}
