package patterns.creational.builder;

public class BrickWorkerBuilder implements Builder {
    private String name;
    private int rang;
    private Specialization specialization;

    public void setName(String name){
        this.name = name;
    }

    public void setRang(int rang){
        this.rang = rang;
    }

    public void setSpecialization(){
        this.specialization = Specialization.BRICK;
    }

    public Worker getWorker(){
        return new Worker(name, rang, specialization);
    }
}
