package patterns.creational.absctractfactory;

public class Main {
    public static void main(String[] args) {
        TransportProducer producer = new FreightTransportProducer();
        Car car = producer.createCar();
        Plane plane = producer.createPlane();

        System.out.println(car.getModel());
        System.out.println(plane.getType());
    }
}
