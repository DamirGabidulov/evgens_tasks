package patterns.creational.absctractfactory;

public class FreightTransportProducer  implements TransportProducer{

    private static class FreightCar implements Car{
        @Override
        public String getModel() {
            return "Грузовая машина Mercedes";
        }
    }

    private static class FreightPlane implements Plane{
        @Override
        public String getType() {
            return "Грузовой самолет Airbus";
        }
    }

    @Override
    public Car createCar() {
        return new FreightCar();
    }

    @Override
    public Plane createPlane() {
        return new FreightPlane();
    }
}
