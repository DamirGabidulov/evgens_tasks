package patterns.creational.absctractfactory;

public class PassengerTranportProducer implements TransportProducer{
    private static class PassengerCar implements Car{
        @Override
        public String getModel() {
            return "Легковая машина Mercedes";
        }
    }

    private static class PassengerPlane implements Plane{
        @Override
        public String getType() {
            return "Легковой самолет Airbus";
        }
    }

    @Override
    public Car createCar() {
        return new PassengerCar();
    }

    @Override
    public Plane createPlane() {
        return new PassengerPlane();
    }
}
