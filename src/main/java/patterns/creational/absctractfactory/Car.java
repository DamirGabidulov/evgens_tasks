package patterns.creational.absctractfactory;

public interface Car {
    String getModel();
}
