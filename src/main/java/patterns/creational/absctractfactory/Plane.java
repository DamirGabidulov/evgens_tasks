package patterns.creational.absctractfactory;

public interface Plane {
    String getType();
}
