package patterns.creational.absctractfactory;

public interface TransportProducer {
    Car createCar();
    Plane createPlane();
}
