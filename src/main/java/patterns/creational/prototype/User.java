package patterns.creational.prototype;

public class User implements Cloneable {

    public enum Role{
        USER, ANON
    }

    public enum State{
        CONFIRMED, NOT_CONFIRMED
    }

    private Role role;
    private State state;
    private String email;

    public User(String email) {
        this.role = Role.ANON;
        this.state = State.NOT_CONFIRMED;
        this.email = email;
    }

    public void authorize(){
        this.role = Role.USER;
        this.state = State.CONFIRMED;
    }

    public boolean isAuthorized(){
        return this.role.equals(Role.USER) && this.state.equals(State.CONFIRMED);
    }

    public User clone(){
        User user = new User(this.email);
        user.state = this.state;
        user.role = this.role;
        return user;
    }
}
