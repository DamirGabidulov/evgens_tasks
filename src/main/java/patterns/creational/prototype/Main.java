package patterns.creational.prototype;

public class Main {
    public static void main(String[] args) {
        User user = new User("group101@gmail.com");
        user.authorize();

        System.out.println(user.isAuthorized());
        User userClone = user.clone();
        System.out.println(userClone.isAuthorized());
    }
}
