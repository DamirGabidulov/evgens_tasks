package patterns.creational.singleton;

public class Main {
    public static void main(String[] args) {
        Diary diary = Diary.getDiary();
        diary.continueNotes("hi");
        diary.continueNotes("my day was nice");
        diary.continueNotes("i met friends today");
        Diary anotherDiary = Diary.getDiary();
        anotherDiary.showDiary();
    }
}
