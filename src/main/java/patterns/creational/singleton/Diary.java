package patterns.creational.singleton;

public class Diary {
    private static Diary diary;
    private static String note = "My Diary \n";

    public Diary() {
    }
    public static synchronized Diary getDiary(){
        if (diary == null){
            diary = new Diary();
        }
        return diary;
    }

    public void continueNotes(String newNote){
        note += newNote + " ";
    }

    public void showDiary(){
        System.out.println(note);
    }
}
