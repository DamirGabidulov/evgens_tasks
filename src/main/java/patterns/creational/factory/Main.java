package patterns.creational.factory;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String from = scanner.nextLine();
        DocumentProducer producer = new StatementProducer();
        Document document = producer.createDocument(from);
        System.out.println(document);
    }
}
