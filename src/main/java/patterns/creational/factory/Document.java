package patterns.creational.factory;

public interface Document {
    String getType();
    String getFrom();
}
