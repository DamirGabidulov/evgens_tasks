package patterns.creational.factory;

public class StatementProducer implements DocumentProducer{
    @Override
    public Document createDocument(String from) {
        return new Statement(from);
    }
}
