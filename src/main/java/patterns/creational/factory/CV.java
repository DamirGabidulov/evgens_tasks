package patterns.creational.factory;

public class CV implements Document{

    private String from;

    public CV(String from) {
        this.from = from;
    }

    @Override
    public String getType() {
        return "Резюме";
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public String toString() {
        return getType() + " от " + getFrom();
    }
}
