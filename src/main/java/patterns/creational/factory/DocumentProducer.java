package patterns.creational.factory;

public interface DocumentProducer {
    Document createDocument(String from);
}
