package patterns.creational.factory;

public class Statement implements Document{

    private String from;

    public Statement(String from) {
        this.from = from;
    }

    @Override
    public String getType() {
        return "Заявление";
    }

    @Override
    public String getFrom() {
        return from;
    }

    @Override
    public String toString() {
        return getType() + " от " + getFrom();
    }
}
