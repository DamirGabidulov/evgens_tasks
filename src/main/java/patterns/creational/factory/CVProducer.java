package patterns.creational.factory;

public class CVProducer implements DocumentProducer{
    @Override
    public Document createDocument(String from) {
        return new CV(from);
    }
}
