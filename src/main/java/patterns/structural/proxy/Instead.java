package patterns.structural.proxy;

public interface Instead {
    void instead();
}
