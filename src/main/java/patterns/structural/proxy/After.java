package patterns.structural.proxy;

public interface After {
    void after();
}
