package patterns.structural.proxy;

public class Main {
    public static void main(String[] args) {
        UserService userService = new UserService();
        UserServiceProxy proxy = new UserServiceProxy(userService);
        proxy.setBefore(() -> System.out.println("Проверка непрочитанных сообщений"));
        //proxy.setInstead(() -> System.out.println("Технические работы"));
        proxy.setAfter(() -> System.out.println("Логирование времени входа"));
        proxy.login("g.damir@gmail.com", "qwerty");
    }
}
