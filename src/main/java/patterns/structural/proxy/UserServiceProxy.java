package patterns.structural.proxy;

public class UserServiceProxy extends UserService{
    private Before before;
    private Instead instead;
    private After after;
    private UserService userService;

    public void setBefore(Before before) {
        this.before = before;
    }

    public void setInstead(Instead instead) {
        this.instead = instead;
    }

    public void setAfter(After after) {
        this.after = after;
    }

    public UserServiceProxy(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void login(String email, String password) {
        if (before != null){
            before.before();
        }
        if (instead != null){
            instead.instead();
        } else {
            userService.login(email, password);
        }
        if (after != null){
            after.after();
        }
    }
}
