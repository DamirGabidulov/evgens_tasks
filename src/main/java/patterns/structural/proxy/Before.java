package patterns.structural.proxy;

public interface Before {
    void before();
}
