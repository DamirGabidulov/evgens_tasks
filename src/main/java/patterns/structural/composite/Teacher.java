package patterns.structural.composite;

public interface Teacher {
    void teach();
}
