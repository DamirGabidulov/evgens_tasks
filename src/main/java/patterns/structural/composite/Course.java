package patterns.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class Course implements Teacher {

    private final List<Teacher> teachers = new ArrayList<>();

    public void addTeacherToCourse(Teacher teacher){
        teachers.add(teacher);
    }

    public void removeTeacherFromCourse(Teacher teacher){
        teachers.remove(teacher);
    }

    @Override
    public void teach() {
        System.out.println("Старт курса");
        for (Teacher teacher : teachers){
            teacher.teach();
        }
    }
}
