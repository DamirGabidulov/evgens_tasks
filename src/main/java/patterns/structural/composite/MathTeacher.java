package patterns.structural.composite;

public class MathTeacher implements Teacher {
    @Override
    public void teach() {
        System.out.println("Урок математики начался");
    }
}
