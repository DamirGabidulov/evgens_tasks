package patterns.structural.composite;

public class Main {
    public static void main(String[] args) {
        Course course = new Course();

        course.addTeacherToCourse(new MathTeacher());
        course.addTeacherToCourse(new GeometryTeacher());

        course.teach();
    }
}
