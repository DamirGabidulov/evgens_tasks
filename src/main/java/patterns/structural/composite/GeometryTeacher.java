package patterns.structural.composite;

public class GeometryTeacher implements Teacher {
    @Override
    public void teach() {
        System.out.println("Урок геометрии начался");
    }
}
