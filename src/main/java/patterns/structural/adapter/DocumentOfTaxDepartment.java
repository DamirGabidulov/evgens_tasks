package patterns.structural.adapter;

public class DocumentOfTaxDepartment {
    public void addNumberOfDepartment(){
        System.out.println("Отправитель: Налоговый участок №6");
    }

    public void addClient(){
        System.out.println("Получатель: Налогоплательщик Иван");
    }

    public void sendToClient(){
        System.out.println("Отправка письма");
    }
}
