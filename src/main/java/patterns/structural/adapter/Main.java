package patterns.structural.adapter;

public class Main {
    public static void main(String[] args) {
        Document document = new AdapterDocumentToTaxDocument();
        document.addSender();
        document.addRecipient();
        document.print();
    }
}
