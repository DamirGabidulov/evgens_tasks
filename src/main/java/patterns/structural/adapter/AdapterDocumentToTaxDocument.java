package patterns.structural.adapter;

public class AdapterDocumentToTaxDocument extends DocumentOfTaxDepartment implements Document {

    @Override
    public void addSender() {
        addNumberOfDepartment();
    }

    @Override
    public void addRecipient() {
        addClient();
    }

    @Override
    public void print() {
        sendToClient();
    }
}
