package patterns.structural.adapter;

public interface Document {
    void addSender();
    void addRecipient();
    void print();
}
