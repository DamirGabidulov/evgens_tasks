package patterns.structural.flyweight;

import java.util.ArrayList;
import java.util.List;

public class Course {
    private final LessonFactory lessonFactory;
    private List<Lesson> lessons = new ArrayList<>();

    public Course(LessonFactory lessonFactory) {
        this.lessonFactory = lessonFactory;
    }

    public void addLesson(String subjectName, int grade){
        lessons.add(lessonFactory.getLesson(subjectName, grade));
    }

    public void getLessons(){
        for (Lesson lesson : lessons){
            System.out.println(lesson.getName());
        }
    }
}
