package patterns.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class LessonFactory {
    private static final Map<String, Lesson> types = new HashMap<>();
    private String name;

    public Lesson getLesson(String subjectName, int grade) {
        Lesson type = types.get(subjectName);
        if (type == null) {
            switch (subjectName) {
                case "math":
                    System.out.println("Создаем новый урок по математике");
                    name = grade + " класс, " + subjectName;
                    type = new Lesson(name, grade, subjectName, "Математика. Мордкович");
                    break;
                case "geometry":
                    System.out.println("Создаем новый урок по геометрии");
                    name = grade + " класс, " + subjectName;
                    type = new Lesson(name, grade, subjectName, "Геометрия. Атанасян");
                    break;
            }
            types.put(subjectName, type);
        }
        return type;
    }
}
