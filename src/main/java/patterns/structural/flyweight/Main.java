package patterns.structural.flyweight;

public class Main {
    public static void main(String[] args) {
        LessonFactory factory = new LessonFactory();
        Course course = new Course(factory);
        course.addLesson("math", 10);
        course.addLesson("math", 10);
        course.addLesson("math", 10);
        course.addLesson("geometry", 11);
        course.addLesson("geometry", 11);
        course.getLessons();
    }
}
