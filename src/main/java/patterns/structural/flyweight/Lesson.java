package patterns.structural.flyweight;

public class Lesson {

    private String name;
    private int grade;
    private String subjectName;
    private String bookName;

    public Lesson(String name, int grade, String subjectName, String bookName) {
        this.name = name;
        this.grade = grade;
        this.subjectName = subjectName;
        this.bookName = bookName;
    }

    public String getName() {
        return name;
    }
}
