package patterns.structural.facade;

public class UserService {
    private EmailAndPasswordValidator validator;
    private EmailNotifier notifier;
    private UserRepository repository;

    public UserService(EmailAndPasswordValidator validator, EmailNotifier notifier, UserRepository repository) {
        this.validator = validator;
        this.notifier = notifier;
        this.repository = repository;
    }

    public void signUp(String email, String password){
        validator.validate(email, password);
        repository.saveUser(new User(email, password));
        notifier.notify(email);
    }
}
