package patterns.structural.facade;

public class UserRepository {
    public void saveUser(User user){
        System.out.println("Сохранение юзера в БД");
    };
}
