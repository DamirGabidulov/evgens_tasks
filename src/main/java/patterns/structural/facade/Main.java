package patterns.structural.facade;

public class Main {
    public static void main(String[] args) {
        EmailAndPasswordValidator validator = new EmailAndPasswordValidator();
        EmailNotifier notifier = new EmailNotifier();
        UserRepository repository = new UserRepository();
        UserService service = new UserService(validator, notifier, repository);
        service.signUp("g.damir@gmail.com", "qwerty");
    }
}
