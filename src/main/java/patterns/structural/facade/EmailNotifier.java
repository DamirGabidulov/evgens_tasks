package patterns.structural.facade;

public class EmailNotifier {
    public void notify(String email){
        System.out.println("Оповещение об регистрации по email - " + email);
    };
}
