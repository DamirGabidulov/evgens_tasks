package patterns.structural.bridge;

public class AdvancersCourse extends Course{
    public AdvancersCourse(Teacher teacher) {
        super(teacher);
    }

    @Override
    public void startCourse() {
        System.out.println("Старт курса для продвинутых");
        teacher.teach();
    }
}
