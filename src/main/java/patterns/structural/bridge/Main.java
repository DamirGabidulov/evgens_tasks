package patterns.structural.bridge;

public class Main {
    public static void main(String[] args) {
        Course[] courses = {
                new BegginersCourse(new EnglishTeacher()),
                new AdvancersCourse(new TatarTeacher())
        };

        for (Course course : courses){
            course.startCourse();
        }
    }
}
