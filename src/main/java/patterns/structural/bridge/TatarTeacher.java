package patterns.structural.bridge;

public class TatarTeacher implements Teacher{
    @Override
    public void teach() {
        System.out.println("Урок татарского начался");
    }
}
