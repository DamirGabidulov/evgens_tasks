package patterns.structural.bridge;

public class BegginersCourse extends Course{
    public BegginersCourse(Teacher teacher) {
        super(teacher);
    }

    @Override
    public void startCourse() {
        System.out.println("Старт курса для начинающих");
        teacher.teach();
    }
}
