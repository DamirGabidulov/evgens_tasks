package patterns.structural.bridge;

public interface Teacher {
    void teach();
}
