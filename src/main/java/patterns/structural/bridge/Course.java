package patterns.structural.bridge;

public abstract class Course {

    protected Teacher teacher;

    public Course(Teacher teacher) {
        this.teacher = teacher;
    }

    public abstract void startCourse();
}
