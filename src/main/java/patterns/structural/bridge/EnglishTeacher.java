package patterns.structural.bridge;

public class EnglishTeacher implements Teacher{
    @Override
    public void teach() {
        System.out.println("Урок английского начался");
    }
}
