package patterns.structural.decorator;

public class TelegramNotifier extends NotifierDecorator{

    private String username;
    private Notifier notifier;

    public TelegramNotifier(Notifier notifier, String username) {
        super(notifier);
        this.username = username;
        this.notifier = notifier;
    }

    @Override
    public void notifyUser(String message) {
        notifier.notifyUser(message);
        System.out.println("Оповещение в Телеграм - " + username  + " сообщение: " + message);
    }
}
