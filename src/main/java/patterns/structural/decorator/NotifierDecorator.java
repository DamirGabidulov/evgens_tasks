package patterns.structural.decorator;

public class NotifierDecorator extends Notifier{

    private final Notifier notifier;

    public NotifierDecorator(Notifier notifier) {
        super(notifier.getEmail());
        this.notifier = notifier;
    }

    @Override
    public void notifyUser(String message) {
        super.notifyUser(message);
    }
}
