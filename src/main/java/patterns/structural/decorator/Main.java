package patterns.structural.decorator;

public class Main {
    public static void main(String[] args) {
        Notifier notifier = new Notifier("g.damir@gmail.com");
        SmsNotifier smsNotifier = new SmsNotifier(notifier, "+12345678910");
        TelegramNotifier telegramNotifier = new TelegramNotifier(smsNotifier, "@damir");
        SlackNotifier slackNotifier = new SlackNotifier(telegramNotifier, "@damir_slack");
        slackNotifier.notifyUser("Hello!");
    }
}
