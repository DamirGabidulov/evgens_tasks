package patterns.structural.decorator;

public class Notifier {
    private String email;

    public Notifier(String email) {
        this.email = email;
    }

    public void notifyUser(String message){
        System.out.println("Оповещение по email - " + email + " сообщение: " + message);
    }

    public String getEmail() {
        return email;
    }
}
