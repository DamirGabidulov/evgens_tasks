package patterns.structural.decorator;

public class SlackNotifier extends NotifierDecorator{

    private String userName;
    private Notifier notifier;

    public SlackNotifier(Notifier notifier, String userName) {
        super(notifier);
        this.userName = userName;
        this.notifier = notifier;
    }

    @Override
    public void notifyUser(String message) {
        notifier.notifyUser(message);
        System.out.println("Оповещение в Slack - " + userName  + " сообщение: " + message);
    }
}
