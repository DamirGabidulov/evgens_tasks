package patterns.structural.decorator;

public class SmsNotifier extends NotifierDecorator{

    private String phoneNumber;
    private Notifier notifier;

    public SmsNotifier(Notifier notifier, String phoneNumber) {
        super(notifier);
        this.phoneNumber = phoneNumber;
        this.notifier = notifier;
    }

    @Override
    public void notifyUser(String message) {
        notifier.notifyUser(message);
        System.out.println("Оповещение по смс - " + phoneNumber  + " сообщение: " + message);
    }
}
