package patterns.behavioral.mediator;

public interface Channel {
    void sendMessage(String message, User user);
}
