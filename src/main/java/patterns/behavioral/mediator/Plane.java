package patterns.behavioral.mediator;

public class Plane implements User{

    private Channel channel;
    private String planeNumber;

    public Plane(Channel channel, String planeNumber) {
        this.channel = channel;
        this.planeNumber = planeNumber;
    }

    @Override
    public String getName() {
        return planeNumber;
    }

    @Override
    public void sendMessage(String message) {
        channel.sendMessage(message, this);
    }

    @Override
    public void getMessage(String message, User user) {
        System.out.println("борт " + this.planeNumber + " получил сообщение: " + message + " от " + user.getName());
    }
}
