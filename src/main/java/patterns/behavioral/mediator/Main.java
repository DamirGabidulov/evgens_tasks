package patterns.behavioral.mediator;

public class Main {
    public static void main(String[] args) {
        OriginChannel channel = new OriginChannel();

        Dispatcher dispatcher = new Dispatcher(channel, "Main dispatcher");
        Plane plane1 = new Plane(channel, "A101");
        Plane plane2 = new Plane(channel, "B202");
        channel.setDispatcher(dispatcher);
        channel.addUser(plane1);
        channel.addUser(plane2);

        plane1.sendMessage("hi");
        dispatcher.sendMessage("roger");
    }
}
