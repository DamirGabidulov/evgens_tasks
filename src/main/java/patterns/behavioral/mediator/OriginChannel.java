package patterns.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;

public class OriginChannel implements Channel{

    private Dispatcher dispatcher;
    private List<User> users = new ArrayList<>();

    public void setDispatcher(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    public void addUser(User user){
        users.add(user);
    }

    @Override
    public void sendMessage(String message, User user) {
        for (User u : users){
            if (u != user){
                u.getMessage(message, user);
            }
        }
        dispatcher.getMessage(message, user);
    }
}
