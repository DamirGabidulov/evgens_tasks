package patterns.behavioral.mediator;

public class Dispatcher implements User{

    private Channel channel;
    private String name;

    public Dispatcher(Channel channel, String name) {
        this.channel = channel;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        channel.sendMessage(message, this);
    }

    @Override
    public void getMessage(String message, User user) {
        System.out.println(this.name + " получил сообщение: " + message + " от " + user.getName());
    }
}
