package patterns.behavioral.state;

import java.util.ArrayList;
import java.util.List;

public class ListState implements State{

    static class KeyValue{
        String key;
        String value;

        public KeyValue(String key, String value) {
            this.key = key;
            this.value = value;
        }
    }

    private final KeyValueStorageImpl keyValueStorageImpl;
    private final static int MAX_SIZE = 7;
    private List<KeyValue> list = new ArrayList<>();

    public ListState(KeyValueStorageImpl keyValueStorageImpl) {
        this.keyValueStorageImpl = keyValueStorageImpl;
    }

    @Override
    public void put(String key, String value) {
        if (list.size() == MAX_SIZE){
            keyValueStorageImpl.state = new FileState(keyValueStorageImpl);
            for (KeyValue keyValue : list){
                keyValueStorageImpl.put(keyValue.key, keyValue.value);
            }
            keyValueStorageImpl.put(key, value);
        } else {
            list.add(new KeyValue(key, value));
        }
    }

    @Override
    public String get(String key) {
        for (KeyValue keyValue : list){
            if (keyValue.key.equals(key)){
                return keyValue.value;
            }
        }
        return null;
    }
}
