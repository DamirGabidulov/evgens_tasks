package patterns.behavioral.state;

import java.util.HashMap;
import java.util.Map;

public class MapState implements State{

    HashMap<String, String> map = new HashMap<>();
    private final KeyValueStorageImpl keyValueStorageImpl;
    private final static int MAX_SIZE = 4;


    public MapState(KeyValueStorageImpl keyValueStorageImpl) {
        this.keyValueStorageImpl = keyValueStorageImpl;
    }

    @Override
    public void put(String key, String value) {
        if (map.size() == MAX_SIZE){
            keyValueStorageImpl.state = new ListState(keyValueStorageImpl);
            for (Map.Entry<String, String> keyValue : map.entrySet()){
                keyValueStorageImpl.put(keyValue.getKey(), keyValue.getValue());
            }
        } else {
            map.put(key, value);
        }
    }

    @Override
    public String get(String key) {
        return map.get(key);
    }
}
