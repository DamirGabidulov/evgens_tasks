package patterns.behavioral.state;

public interface State {
    void put(String key, String value);
    String get(String key);
}
