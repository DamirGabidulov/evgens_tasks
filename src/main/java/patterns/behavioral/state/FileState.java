package patterns.behavioral.state;

import java.io.*;

public class FileState implements State{

    private final KeyValueStorageImpl keyValueStorageImpl;

    public FileState(KeyValueStorageImpl keyValueStorageImpl) {
        this.keyValueStorageImpl = keyValueStorageImpl;
    }


    @Override
    public void put(String key, String value) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter("stateData.txt", true))){
            writer.write(key + "=" + value);
            writer.newLine();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }

    @Override
    public String get(String key) {
        try (BufferedReader reader = new BufferedReader(new FileReader("stateData.txt"))){
            String line = reader.readLine();
            while (line != null){
                String[] strings = line.split("=");

                if (strings[0].equals(key)){
                    return strings[1];
                }

                line = reader.readLine();
            }
            return null;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
