package patterns.behavioral.state;

import java.util.StringJoiner;

public class KeyValueStorageImpl implements KeyValueStorage {

    State state;

    public KeyValueStorageImpl() {
        this.state = new MapState(this);
    }

    public void put(String key, String value){
        state.put(key, value);
    }

    public String get(String key){
     return state.get(key);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", KeyValueStorageImpl.class.getSimpleName() + "[", "]")
                .add("state: " + state.getClass().getSimpleName())
                .toString();
    }
}
