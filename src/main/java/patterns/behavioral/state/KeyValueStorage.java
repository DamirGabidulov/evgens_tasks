package patterns.behavioral.state;

public interface KeyValueStorage {

    void put(String key, String value);
    String get(String key);
}
