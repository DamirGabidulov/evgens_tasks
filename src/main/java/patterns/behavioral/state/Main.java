package patterns.behavioral.state;

public class Main {
    public static void main(String[] args) {
        KeyValueStorageImpl keyValueStorage = new KeyValueStorageImpl();
        keyValueStorage.put("1", "pass1");
        keyValueStorage.put("2", "pass2");
        keyValueStorage.put("3", "pass3");
        keyValueStorage.put("4", "pass4");
        System.out.println(keyValueStorage.get("4"));
        System.out.println(keyValueStorage.toString());

        //состояние поменяется
        keyValueStorage.put("5", "pass5");
        keyValueStorage.put("6", "pass6");
        keyValueStorage.put("7", "pass7");
        System.out.println(keyValueStorage.get("7"));
        System.out.println(keyValueStorage.toString());

        //состояние поменяется
        keyValueStorage.put("8", "pass8");
        keyValueStorage.put("9", "pass9");
        System.out.println(keyValueStorage.get("8"));
        System.out.println(keyValueStorage.toString());
    }
}
