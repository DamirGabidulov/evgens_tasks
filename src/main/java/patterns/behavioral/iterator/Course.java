package patterns.behavioral.iterator;

public class Course {
    private String name;
    private String[] lessons;

    public Course(String name, String[] lessons) {
        this.name = name;
        this.lessons = lessons;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getLessons() {
        return lessons;
    }

    public void setLessons(String[] lessons) {
        this.lessons = lessons;
    }

    public Iterator getIterator(){
        return new LessonIterator();
    }

    private class LessonIterator implements Iterator{

        int index;

        @Override
        public boolean hasNext() {
            if (index < lessons.length){
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            return lessons[index++];
        }
    }
}
