package patterns.behavioral.iterator;

public class Main {
    public static void main(String[] args) {
        String[] lessons = {"Math", "Geometry", "Biology", "English"};
        Course course = new Course("School course", lessons);

        Iterator iterator = course.getIterator();
        System.out.println("School program's subjects:");

        while (iterator.hasNext()){
            System.out.println(iterator.next().toString());
        }

    }
}
