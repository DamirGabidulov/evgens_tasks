package patterns.behavioral.inerpreter;

public class Main {
    public static void main(String[] args) {

        Expression isGraduated = getLevel();
        Expression isGoodInSubjects = getSubject();

        System.out.println("Высшее образование у претендента - " + isGraduated.interpret("master"));
        System.out.println("Хорошие знания в математике и английском у претендента - " + isGoodInSubjects.interpret("english algebra"));

    }

    public static Expression getLevel(){
        Expression beginner = new TerminalExpression("bachelor");
        Expression advanced = new TerminalExpression("master");

        return new OrExpression(beginner, advanced);
    }

    public static Expression getSubject(){
        Expression math = new TerminalExpression("algebra");
        Expression language = new TerminalExpression("english");

        return new AndExpression(math, language);
    }
}
