package patterns.behavioral.inerpreter;

public interface Expression {
    boolean interpret(String context);
}
