package patterns.behavioral.chain;

public class Main {
    public static void main(String[] args) {
        UserRepository userRepository = new UserRepository();
        NotNullHandler notNullHandler = new NotNullHandler();
        EmailHandler emailHandler = new EmailHandler();
        PasswordHandler passwordHandler = new PasswordHandler();

        notNullHandler.setNext(emailHandler);
        emailHandler.setNext(passwordHandler);

        UserService userService = new UserService(notNullHandler, userRepository);
        userService.signIn("g.damir@gmail.com", "qwerty007");
    }
}
