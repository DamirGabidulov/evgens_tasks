package patterns.behavioral.chain;

public abstract class BaseHandler {

    protected BaseHandler next;

    public void setNext(BaseHandler next) {
        this.next = next;
    }

    public abstract void nextHandle(String email, String password);
}
