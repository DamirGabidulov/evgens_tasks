package patterns.behavioral.chain;

public class EmailHandler extends BaseHandler{


    @Override
    public void nextHandle(String email, String password) {
        if (email.contains("@")){
            if (next != null){
                next.nextHandle(email, password);
            }
        } else {
            throw new IllegalArgumentException("Неправильный формат email");
        }
    }
}
