package patterns.behavioral.chain;

public class UserService {

    private BaseHandler handler;
    private UserRepository repository;

    public UserService(BaseHandler handler, UserRepository repository) {
        this.handler = handler;
        this.repository = repository;
    }

    public void signIn(String email, String password){
        handler.nextHandle(email, password);
        User user = repository.findByEmail(email);
        if (user != null && password.equals("qwerty007")){
            System.out.println("Успешная аутентификация");
        } else {
            System.err.println("Пользователь не найден");
        }
    }
}
