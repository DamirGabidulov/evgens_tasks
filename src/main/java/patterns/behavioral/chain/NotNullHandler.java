package patterns.behavioral.chain;

public class NotNullHandler extends BaseHandler{
    @Override
    public void nextHandle(String email, String password) {
        if (email == null || password == null){
            throw new IllegalArgumentException("Пустой пароль или email");
        } else {
            next.nextHandle(email, password);
        }
    }
}
