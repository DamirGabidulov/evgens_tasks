package patterns.behavioral.chain;

public class UserRepository {
    public User findByEmail(String email){
        if (email.equals("g.damir@gmail.com")){
            return new User(email, "qwerty007");
        }
        return null;
    }
}
