package patterns.behavioral.chain;

public class PasswordHandler extends BaseHandler{
    @Override
    public void nextHandle(String email, String password) {
        if (password.length() > 7){
            if (next != null){
                next.nextHandle(email, password);
            }
        } else {
            throw new IllegalArgumentException("Неправильная длина пароля");
        }
    }
}
