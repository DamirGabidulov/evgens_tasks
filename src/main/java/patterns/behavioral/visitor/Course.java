package patterns.behavioral.visitor;

public class Course implements CourseElement{

    private CourseElement[] elements;

    public Course() {
        this.elements = new CourseElement[]{
                new EnglishLesson(),
                new MathLesson()
        };
    }

    @Override
    public void start(Teacher teacher) {
        for (CourseElement element : elements){
            element.start(teacher);
        }
    }
}
