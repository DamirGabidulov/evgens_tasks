package patterns.behavioral.visitor;

public class MediumsTeacher implements Teacher{
    @Override
    public void teach(EnglishLesson englishLesson) {
        System.out.println("Урок по английскому для среднего уровня начался");
    }

    @Override
    public void teach(MathLesson mathLesson) {
        System.out.println("Урок по математике для среднего уровня начался");
    }
}
