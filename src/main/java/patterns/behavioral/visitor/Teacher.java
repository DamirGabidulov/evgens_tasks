package patterns.behavioral.visitor;

public interface Teacher {
    void teach(EnglishLesson englishLesson);
    void teach(MathLesson mathLesson);
}
