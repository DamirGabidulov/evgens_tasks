package patterns.behavioral.visitor;

public class BeginnersTeacher implements Teacher {

    @Override
    public void teach(EnglishLesson englishLesson) {
        System.out.println("Урок по английскому для начального уровня начался");
    }

    @Override
    public void teach(MathLesson mathLesson) {
        System.out.println("Урок по математике для начального уровня начался");
    }
}
