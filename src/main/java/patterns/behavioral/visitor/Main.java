package patterns.behavioral.visitor;

public class Main {
    public static void main(String[] args) {
        Course course = new Course();
        Teacher beginnersTeacher = new BeginnersTeacher();
        Teacher mediumTeacher = new MediumsTeacher();

        System.out.println("Младшая школа");
        course.start(beginnersTeacher);
        System.out.println("Средняя школа");
        course.start(mediumTeacher);
    }
}
