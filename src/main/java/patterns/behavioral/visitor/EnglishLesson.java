package patterns.behavioral.visitor;

public class EnglishLesson implements CourseElement{

    @Override
    public void start(Teacher teacher) {
        teacher.teach(this);
    }
}
