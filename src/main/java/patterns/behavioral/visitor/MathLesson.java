package patterns.behavioral.visitor;

public class MathLesson implements CourseElement{
    @Override
    public void start(Teacher teacher) {
        teacher.teach(this);
    }
}
