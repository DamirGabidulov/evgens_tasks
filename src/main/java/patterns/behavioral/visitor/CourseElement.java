package patterns.behavioral.visitor;

public interface CourseElement {
    void start(Teacher teacher);
}
