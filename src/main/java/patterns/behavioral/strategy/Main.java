package patterns.behavioral.strategy;

public class Main {
    public static void main(String[] args) {
        Sequence sequence = new Sequence(new int[]{12, 4, 5, 10, -100, -1});
        sequence.setSort(new SelectionSort());
        sequence.sort();
        sequence.print();
    }
}
