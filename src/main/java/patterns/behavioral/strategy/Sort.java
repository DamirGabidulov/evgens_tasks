package patterns.behavioral.strategy;

public interface Sort {
    void sort(int[] array);
}
