package patterns.behavioral.strategy;

import java.util.Arrays;

public class Sequence {

    private int[] array;
    private Sort sort;

    public Sequence(int[] array) {
        this.array = new int[array.length];
        System.arraycopy(array, 0, this.array, 0, array.length);
    }

    public void print(){
        System.out.println(Arrays.toString(this.array));
    }

    public void sort(){
        sort.sort(this.array);
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }
}
