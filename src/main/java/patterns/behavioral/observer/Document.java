package patterns.behavioral.observer;

public interface Document {
    String getText();
}
