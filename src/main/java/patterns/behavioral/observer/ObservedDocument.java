package patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

//Subject
public class ObservedDocument {
    private List<DocumentHandler> handlers;

    public ObservedDocument() {
        this.handlers = new ArrayList<>();
    }

    void addHandlers(DocumentHandler handler){
        handlers.add(handler);
    }

    void notifyHandlers(Document document){
        for (DocumentHandler handler : handlers){
            handler.handle(document);
        }
    }
}
