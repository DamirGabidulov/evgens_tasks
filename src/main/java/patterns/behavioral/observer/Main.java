package patterns.behavioral.observer;

public class Main {
    public static void main(String[] args) {
        ObservedDocument observedDocument = new ObservedDocument();

        observedDocument.addHandlers(document -> System.out.println("Документ '" + document.getText() + "' отправлен в отдел расследований"));
        observedDocument.addHandlers(document -> System.out.println("Документ '" + document.getText() + "' отправлен в отдел нравов"));
        observedDocument.addHandlers(document -> System.out.println("Документ '" + document.getText() + "' отправлен в отдел поведенческого анализа"));

        observedDocument.notifyHandlers(new TextDocument("Дело 101 долматинца"));
    }
}
