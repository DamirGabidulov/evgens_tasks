package patterns.behavioral.observer;

//Observer
public interface DocumentHandler {
    void handle(Document document);
}
