package patterns.behavioral.observer;

public class TextDocument implements Document{

    private String text;

    public TextDocument(String text) {
        this.text = text;
    }

    @Override
    public String getText() {
        return text;
    }
}
