package patterns.behavioral.template;

public class Main {
    public static void main(String[] args) {
        Network twitter = new Twitter("g.damir@gmail.com", "123456");
        twitter.createPost();
        Network instagram = new Instagram("hello@mail.ru", "123");
        instagram.createPost();
    }
}
