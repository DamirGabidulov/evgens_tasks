package patterns.behavioral.template;

public abstract class Network {

    private String email;
    private String password;

    public Network(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public void createPost(){
        if (logIn()){
            writeNote();
        } else {
            System.out.println("Wrong password or email. Please login in " + this.getClass().getSimpleName() + " again");
        }
    }

    private boolean logIn(){
        return email.contains("@") & password.length() > 5;
    }
    public abstract void writeNote();
}
