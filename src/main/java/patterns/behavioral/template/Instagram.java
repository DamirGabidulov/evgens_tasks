package patterns.behavioral.template;

public class Instagram extends Network{

    public Instagram(String email, String password) {
        super(email, password);
    }

    @Override
    public void writeNote() {
        System.out.println("Post in Instagram created");
    }
}
