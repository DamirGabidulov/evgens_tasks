package patterns.behavioral.template;

public class Twitter extends Network{


    public Twitter(String email, String password) {
        super(email, password);
    }

    @Override
    public void writeNote() {
        System.out.println("Post in Twitter created");
    }
}
