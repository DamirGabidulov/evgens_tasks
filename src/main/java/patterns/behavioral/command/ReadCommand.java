package patterns.behavioral.command;

public class ReadCommand implements Command{
    private TextEditor textEditor;

    public ReadCommand(TextEditor textEditor) {
        this.textEditor = textEditor;
    }

    @Override
    public void execute() {
        textEditor.read();
    }
}
