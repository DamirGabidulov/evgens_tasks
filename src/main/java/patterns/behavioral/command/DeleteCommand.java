package patterns.behavioral.command;

public class DeleteCommand implements Command{
    private TextEditor textEditor;

    public DeleteCommand(TextEditor textEditor) {
        this.textEditor = textEditor;
    }

    @Override
    public void execute() {
        textEditor.delete();
    }
}
