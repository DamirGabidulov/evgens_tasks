package patterns.behavioral.command;

public class User {
    private Command write;
    private Command read;
    private Command delete;

    public User(Command write, Command read, Command delete) {
        this.write = write;
        this.read = read;
        this.delete = delete;
    }

    public void write(){
        write.execute();
    }

    public void read(){
        read.execute();
    }

    public void delete(){
        delete.execute();
    }
}
