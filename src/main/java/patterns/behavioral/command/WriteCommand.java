package patterns.behavioral.command;

public class WriteCommand implements Command{

    private TextEditor textEditor;

    public WriteCommand(TextEditor textEditor) {
        this.textEditor = textEditor;
    }

    @Override
    public void execute() {
        textEditor.write();
    }
}
