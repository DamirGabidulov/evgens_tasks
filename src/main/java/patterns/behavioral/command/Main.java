package patterns.behavioral.command;

public class Main {
    public static void main(String[] args) {
        TextEditor textEditor = new TextEditor();

        User user = new User(new WriteCommand(textEditor),
                new ReadCommand(textEditor),
                new DeleteCommand(textEditor));

        user.write();
        user.read();
        user.delete();
    }
}
