package patterns.behavioral.memento;

public class Main {
    public static void main(String[] args) {
        Game game = new Game();
        File file = new File();

        System.out.println("Playing game");
        game.setLevelAndDate("1 Level");

        System.out.println(game);
        System.out.println("Saving game");
        file.setSave(game.save());

        System.out.println("Continue playing game");
        game.setLevelAndDate("2 Level");
        System.out.println(game);

        System.out.println("Met hard boss, want to replay");
        game.load(file.getLastSave());
        System.out.println(game);
    }
}
