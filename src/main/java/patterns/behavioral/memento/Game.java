package patterns.behavioral.memento;

import java.util.Date;

public class Game {
    private String level;
    private Date date;

    public Save save(){
        return new Save(level);
    }

    public void load(Save save){
        this.level = save.getLevel();
        this.date = save.getDate();
    }

    public void setLevelAndDate(String level) {
        this.level = level;
        this.date = new Date();
    }

    @Override
    public String toString() {
        return "Game{" +
                "level='" + level + '\'' +
                ", date=" + date +
                '}';
    }
}
