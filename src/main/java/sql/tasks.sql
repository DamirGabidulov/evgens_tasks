Create table Person (PersonId int, FirstName varchar(255), LastName varchar(255));
Create table Address (AddressId int, PersonId int, City varchar(255), State varchar(255));
Truncate table Person;
insert into Person (PersonId, LastName, FirstName) values ('1', 'Wang', 'Allen');
insert into Person (PersonId, LastName, FirstName) values ('3', 'Johnny', 'Cash');
Truncate table Address;
insert into Address (AddressId, PersonId, City, State) values ('1', '2', 'New York City', 'New York');
insert into Address (AddressId, PersonId, City, State) values ('1', '1', 'Kazan', 'RT');

--задача 1
--получить все данные о человеке, не зависимо от того есть у него адрес или нет
select FirstName, LastName, City, State
from Person as p
         LEFT JOIN Address a on a.PersonId = p.PersonId;

Create table If Not Exists Employee (Id int, Salary int);
Truncate table Employee;
insert into Employee (Id, Salary) values ('1', '100');
insert into Employee (Id, Salary) values ('2', '200');
insert into Employee (Id, Salary) values ('3', '300');

--задача 2
--ВЕРНУТЬ ВТОРУЮ ПО ВЕЛИЧИНЕ ЗАРПЛАТУ
SELECT Salary
FROM Employee
ORDER BY Salary DESC
    LIMIT 1 OFFSET 1;

Create table If Not Exists Employee (Id int, Name varchar(255), Salary int, ManagerId int);
Truncate table Employee;
insert into Employee (Id, Name, Salary, ManagerId) values ('1', 'Joe', '70000', '3');
insert into Employee (Id, Name, Salary, ManagerId) values ('2', 'Henry', '80000', '4');
insert into Employee (Id, Name, Salary, ManagerId) values ('3', 'Sam', '60000', NULL);
insert into Employee (Id, Name, Salary, ManagerId) values ('4', 'Max', '90000', NULL);

--задача 3
--вернуть имя работника у которого менеджер зарабатывает меньше чем он сам
select Name
from Employee as e
where Salary > (
    select Salary
    from Employee as x
    where e.ManagerId = x.Id
);

Create table If Not Exists Person (Id int, Email varchar(255));
Truncate table Person;
insert into Person (Id, Email) values ('1', 'a@b.com');
insert into Person (Id, Email) values ('2', 'c@d.com');
insert into Person (Id, Email) values ('3', 'a@b.com');

drop table person;

--задача 4
--выбрать все email которые повторяются
select Email
from Person
group by Email
having count(*) > 1;


Create table If Not Exists Customers (Id int, Name varchar(255));
Create table If Not Exists Orders (Id int, CustomerId int);
Truncate table Customers;
insert into Customers (Id, Name) values ('1', 'Joe');
insert into Customers (Id, Name) values ('2', 'Henry');
insert into Customers (Id, Name) values ('3', 'Sam');
insert into Customers (Id, Name) values ('4', 'Max');
Truncate table Orders;
insert into Orders (Id, CustomerId) values ('1', '3');
insert into Orders (Id, CustomerId) values ('2', '1');

--задача 5
--выбрать клиентов которые не делали заказ
select Name
from Customers
where Id not in (
    select Customers.Id
    from Customers
             join Orders on Customers.Id = Orders.CustomerId
);


Create table If Not Exists courses (student varchar(255), class varchar(255));
Truncate table courses;
insert into courses (student, class) values ('A', 'Math');
insert into courses (student, class) values ('B', 'English');
insert into courses (student, class) values ('C', 'Math');
insert into courses (student, class) values ('D', 'Biology');
insert into courses (student, class) values ('E', 'Math');
insert into courses (student, class) values ('F', 'Computer');
insert into courses (student, class) values ('G', 'Math');
insert into courses (student, class) values ('H', 'Math');
insert into courses (student, class) values ('I', 'Math');

--задача 6
--выбрать классы которые имеют 5 и более учеников
select class
from courses
group by class
having count(*) >= 5;

