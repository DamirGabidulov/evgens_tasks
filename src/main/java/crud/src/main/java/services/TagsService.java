package services;

import models.Tag;
import repositories.TagRepository;

import java.util.List;

public class TagsService {
    private final TagRepository tagRepository;

    public TagsService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    /**
     *
     * @param data - Должно приходить в формате "1, something", где 1 - это id поста; something - это новое имя тега
     * @value postId - id поста которому добавили новый тег
     * @value name - название тега
     */
    public void save(String data) {
        String[] splitedData = data.split(", ");
        if (splitedData[0].matches("\\d+")) {
            Long postId = Long.valueOf(splitedData[0]);
            String name = splitedData[1];
            Tag tag = Tag.builder()
                    .name(name)
                    .build();
            tagRepository.save(postId, tag);
            System.out.println(tag);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     *
     * @return - список всех тегов
     * не используется
     */
    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    /**
     *
     * @param data - id поста у которого хотим получить все теги
     * @return - список тегов
     */
    public List<Tag> findAllByPostId(String data){
        if (data.matches("\\d+")){
            Long postId = Long.valueOf(data);
            return tagRepository.findAllByPostId(postId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     *
     * @param data - Должно приходить в формате "previousName, updatedName", где previousName - это старое название тега; updatedName - это новое имя тега
     */
    public void update(String data){
        String[] splitedData = data.split(", ");
        String previousName = splitedData[0];
        String updatedName = splitedData[1];
        tagRepository.updateName(previousName, updatedName);
        System.out.println("Updated");
    }

    /**
     *
     * @param data - id тега который нужно удалить
     */
    public void delete(String data) {
        if (data.matches("\\d+")){
            Long tagId = Long.valueOf(data);
            tagRepository.delete(tagId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }
}
