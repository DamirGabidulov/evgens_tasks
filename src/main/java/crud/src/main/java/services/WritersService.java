package services;

import models.Writer;
import repositories.PostRepository;
import repositories.WritersRepository;

import java.util.Optional;

public class WritersService {

    private final WritersRepository writersRepository;
    private final PostRepository postRepository;

    public WritersService(WritersRepository writersRepository, PostRepository postRepository) {
        this.writersRepository = writersRepository;
        this.postRepository = postRepository;
    }


    /**
     *
     * @param data - от пользователя в консоли приходит только id Writer-a
     * проверяет существует ли Writer с таким id и печатает его
     */
    public void findById(String data) {
        if (data.matches("\\d+")){
            Long writerId = Long.valueOf(data);
            Optional<Writer> writer = writersRepository.findById(writerId);
            if (writer.isPresent()){
                Writer expectedWriter = writer.get();
                expectedWriter.setPosts(postRepository.findAllByWriterId(writerId));
                System.out.println(expectedWriter);
            } else {
                throw new IllegalArgumentException("Can't find writer with this id");
            }
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     *
     * @param name - поменять можно только имя у Writer-a
     */
    public void save(String name) {
        Writer writer = Writer.builder()
                .name(name)
                .build();
        writersRepository.save(writer);
        System.out.println(writer);
    }

    /**
     *
      * @param data - Должно приходить в формате "1, something", где 1 - это id Writer-a; something - это новое имя Writer-a
     *  Проверяет формат введенных данных
     */
    public void update(String data) {
        String[] splitedData = data.split(", ");
        if (splitedData[0].matches("\\d+")) {
            Long writerId = Long.valueOf(splitedData[0]);
            String updatedContent = splitedData[1];
            writersRepository.update(writerId, updatedContent);
            System.out.println("Updated");
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     *
     * @param data - id Writer-a
     * Удаление Writer-a из базы
     */
    public void delete(String data) {
        if (data.matches("\\d+")){
            Long writerId = Long.valueOf(data);
            writersRepository.delete(writerId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
        System.out.println("Deleted");
    }
}
