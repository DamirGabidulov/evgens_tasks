package services;

import models.Post;
import models.PostStatus;
import repositories.PostRepository;
import repositories.TagRepository;

import java.util.List;
import java.util.Scanner;

public class PostsService {

    private final PostRepository postRepository;
    private final TagRepository tagRepository;

    public PostsService(PostRepository postRepository, TagRepository tagRepository) {
        this.postRepository = postRepository;
        this.tagRepository = tagRepository;
    }


    /**
     *
     * @param data - Должно приходить в формате "1, something", где 1 - это id Writer-a; something - это content
     * @value writerId - id Writer-a который создал пост
     * @value content - контент поста
     */
    public void save(String data) {
        String[] splitedData = data.split(", ");
        if (splitedData[0].matches("\\d+")){
            Long writerId = Long.valueOf(splitedData[0]);
            String content = splitedData[1];

            Post post = Post.builder()
                    .status(PostStatus.ACTIVE)
                    .content(content)
                    .build();
            postRepository.save(writerId, post);
            System.out.println(post);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
    }

    /**
     *
     * @return - список всех постов
     */
    public List<Post> findAll(){
        List<Post> posts = postRepository.findAll();
        for (Post post : posts){
            post.setTags(tagRepository.findAllByPostId(post.getId()));
        }
        return posts;
    }

    /**
     *
     * @param data - Должно приходить в формате "1, something", где 1 - это id поста; something - это новый контент поста
     * @value postId - id поста
     * @value updatedContent - новый контент поста
     */
    public void update(String data) {
        String[] splitedData = data.split(", ");
        Long postId = Long.valueOf(splitedData[0]);
        String updatedContent = splitedData[1];
        postRepository.update(postId, updatedContent);
        System.out.println("Updated");
    }

    /**
     *
     * @param data - id поста который нужно удалить
     * пост не удаляет физически из базы, только ставится статус DELETED. Далее посты с таким статусом в поиске не высвечиваются
     */
    public void delete(String data) {
        if (data.matches("\\d+")){
            Long postId = Long.valueOf(data);
            postRepository.delete(postId);
        } else {
            throw new IllegalArgumentException("Wrong number format");
        }
        System.out.println("Deleted");
    }
}
