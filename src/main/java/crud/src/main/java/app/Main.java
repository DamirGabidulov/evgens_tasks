package app;

import controllers.PostsController;
import controllers.TagsController;
import controllers.WritersController;
import repositories.*;
import services.PostsService;
import services.TagsService;
import services.WritersService;
import view.GeneralView;

import java.sql.*;
import java.util.Scanner;

public class Main {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/test_db";
    private static final String DB_USER = "root";
    private static final String DB_PASSWORD = "mysql";


    public static void main(String[] args) {

        System.out.println("Enter your action, command and data");
        Scanner scanner = new Scanner(System.in);


        try (Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD)){
            PostRepository postRepository = new PostRepositoryImpl(connection);
            TagRepository tagRepository = new TagRepositoryImpl(connection);
            WritersRepository writersRepository = new WritersRepositoryImpl(connection);

            PostsService postsService = new PostsService(postRepository, tagRepository);
            TagsService tagsService = new TagsService(tagRepository);
            WritersService writersService = new WritersService(writersRepository, postRepository);

            PostsController postsController = new PostsController(postRepository, postsService);
            TagsController tagsController = new TagsController(tagsService);
            WritersController writersController = new WritersController(writersService);
            GeneralView generalView = new GeneralView(postsController, tagsController, writersController);


            while (true){
                String action = scanner.nextLine();
                String command = scanner.nextLine();
                String data = scanner.nextLine();
                generalView.doAction(action, command, data);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }



    }
}
