package view;

import controllers.PostsController;
import controllers.TagsController;
import controllers.WritersController;

public class GeneralView {

    private final PostsController postsController;
    private final TagsController tagsController;

    private final WritersController writersController;

    public GeneralView(PostsController postsController, TagsController tagsController, WritersController writersController) {
        this.postsController = postsController;
        this.tagsController = tagsController;
        this.writersController = writersController;
    }

    /**
     *
     * @param action - всего три вида объектов над которым можно проводить действия. Это writer / post / tag
     * @param command - всего четыре вида команд. Это create / read / update / delete
     *                Команды для каждого контроллера одинаковы
     * @param data - зависит от команды, это может быть id, новое имя или контент. Подробнее смотри документацию в сервисах
     */
    public void doAction(String action, String command, String data) {
        switch (action) {
            case "writer":
                writersController.doAction(command, data);
                break;
            case "post":
                postsController.doAction(command, data);
                break;
            case "tag":
                tagsController.doAction(command, data);
                break;
        }
    }
}
