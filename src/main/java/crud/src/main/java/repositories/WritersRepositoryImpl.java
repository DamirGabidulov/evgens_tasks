package repositories;

import models.Writer;

import java.sql.*;
import java.util.Optional;
import java.util.function.Function;

public class WritersRepositoryImpl implements WritersRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from writer where id = ?";

    //language=SQL
    private static final String SQL_SAVE = "insert into writer (name) values (?)";

    //language=SQL
    private static final String SQL_UPDATE = "update writer set name = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE = "delete from writer where id = ?";
    private final Connection connection;

    public WritersRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    private static final Function<ResultSet, Writer> writerMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            return Writer.builder()
                    .id(id)
                    .name(name)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };

    @Override
    public void save(Writer writer) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, writer.getName());

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                if (resultSet.next()) {
                    writer.setId(resultSet.getLong(1));
                } else {
                    throw new SQLException("Can't get id");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Optional<Writer> findById(Long writerId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            preparedStatement.setLong(1, writerId);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return Optional.of(writerMapper.apply(resultSet));
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update(Long writerId, String updatedContent) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)) {
            preparedStatement.setString(1, updatedContent);
            preparedStatement.setLong(2, writerId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long writerId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)){
            preparedStatement.setLong(1, writerId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't delete writer");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
