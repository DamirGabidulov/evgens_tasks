package repositories;

import models.Post;
import models.PostStatus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class PostRepositoryImpl implements PostRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from post where post_status = ?";

    //language=SQL
    private static final String SQL_SELECT_BY_WRITER_ID = "select * from post where writer_id = ? and post_status = ?";

    //language=SQL
    private static final String SQL_SAVE = "insert into post (content, post_status, writer_id) values (?, ?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update post set content = ? where id = ?";

    //language=SQL
    private static final String SQL_DELETE = "update post set post_status = ? where id = ?";

    private final Connection connection;

    public PostRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    private static final Function<ResultSet, Post> postMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String content = resultSet.getString("content");
            String status = resultSet.getString("post_status");
            return Post.builder()
                    .id(id)
                    .content(content)
                    .status(PostStatus.valueOf(status))
                    .build();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public void save(Long writerId, Post post) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE, Statement.RETURN_GENERATED_KEYS)){
            preparedStatement.setString(1,post.getContent());
            preparedStatement.setString(2, post.getStatus().toString());
            preparedStatement.setLong(3, writerId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't insert data");
            }

            try(ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    post.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Can't get id");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public List<Post> findAll() {
        List<Post> posts = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(SQL_SELECT_ALL)){
            statement.setString(1, PostStatus.ACTIVE.toString());

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                posts.add(postMapper.apply(resultSet));
            }
            return posts;

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public List<Post> findAllByWriterId(Long writerId) {
        List<Post> posts = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_BY_WRITER_ID)){
            preparedStatement.setLong(1, writerId);
            preparedStatement.setString(2, PostStatus.ACTIVE.toString());

            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()){
                    posts.add(postMapper.apply(resultSet));
                }
            }
            return posts;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void update(Long postId, String updatedContent) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)){
            preparedStatement.setString(1, updatedContent);
            preparedStatement.setLong(2, postId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't update post");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long postId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)){
            preparedStatement.setString(1, PostStatus.DELETED.toString());
            preparedStatement.setLong(2, postId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't delete post");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
