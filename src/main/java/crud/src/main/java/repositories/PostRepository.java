package repositories;

import models.Post;

import java.util.List;

public interface PostRepository extends GenericRepository<Post, Long>{
    List<Post> findAll();

    List<Post> findAllByWriterId(Long writerId);

    void save(Long writerId, Post post);

    void update(Long postId, String updatedContent);

    void delete(Long postId);
}
