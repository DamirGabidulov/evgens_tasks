package repositories;

import models.Writer;

import java.util.Optional;

public interface WritersRepository extends GenericRepository<Writer, Long>{
    Optional<Writer> findById (Long writerId);

    void save(Writer writer);

    void update(Long writerId, String updatedContent);

    void delete(Long writerId);
}
