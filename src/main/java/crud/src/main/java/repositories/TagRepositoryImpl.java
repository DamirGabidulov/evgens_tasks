package repositories;

import models.Tag;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class TagRepositoryImpl implements TagRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from tag";

    //language=SQL
    private static final String SQL_SELECT_ALL_BY_POST_ID = "select * from tag where post_id = ?";

    //language=SQL
    private static final String SQL_SAVE_TAG = "insert into tag (name, post_id) values (?, ?)";

    //language=SQL
    private static final String SQL_UPDATE = "update tag set name = ? where name = ?";

    //language=SQL
    private static final String SQL_DELETE = "delete from tag where id = ?";

    private final Connection connection;

    public TagRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    private static final Function<ResultSet, Tag> tagsMapper = resultSet -> {
        try {
            Long id = resultSet.getLong("id");
            String name = resultSet.getString("name");
            return Tag.builder()
                    .id(id)
                    .name(name)
                    .build();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    };

    @Override
    public void save(Long postId, Tag tag) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_TAG, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, tag.getName());
            preparedStatement.setLong(2, postId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't insert data");
            }

            try(ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    tag.setId(generatedKeys.getLong(1));
                } else {
                    throw new SQLException("Can't get id");
                }
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Tag> findAll() {
        List<Tag> tags = new ArrayList<>();

        try (Statement statement = connection.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL)) {
                while (resultSet.next()) {
                    tags.add(tagsMapper.apply(resultSet));
                }
            }
            return tags;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Tag> findAllByPostId(Long postId) {
        List<Tag> tags = new ArrayList<>();
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_BY_POST_ID)) {
            preparedStatement.setLong(1, postId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    tags.add(tagsMapper.apply(resultSet));
                }
            }
            return tags;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void updateName(String oldName, String newName) {
        try(PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE)){

            preparedStatement.setString(1, newName);
            preparedStatement.setString(2, oldName);

            int affectedRows = preparedStatement.executeUpdate();
            // здесь 0 так как результатов может быть от одного и больше
            if (affectedRows == 0){
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Long tagId) {
        try (PreparedStatement preparedStatement = connection.prepareStatement(SQL_DELETE)){
            preparedStatement.setLong(1, tagId);

            int affectedRows = preparedStatement.executeUpdate();
            if (affectedRows != 1){
                throw new SQLException("Can't delete tag");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
