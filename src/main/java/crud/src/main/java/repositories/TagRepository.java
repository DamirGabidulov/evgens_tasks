package repositories;

import models.Tag;

import java.util.List;

public interface TagRepository extends GenericRepository<Tag, Long> {
    void save(Long postId, Tag tag);

    List<Tag> findAll();

    List<Tag> findAllByPostId(Long postId);

    void updateName(String oldName, String newName);

    void delete(Long tagId);
}
