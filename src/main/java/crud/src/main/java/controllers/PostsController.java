package controllers;

import models.Post;
import repositories.PostRepository;
import services.PostsService;

import java.util.List;

public class PostsController {
    private final PostRepository postRepository;
    private final PostsService postsService;

    public PostsController(PostRepository postRepository, PostsService postsService) {
        this.postRepository = postRepository;
        this.postsService = postsService;
    }

    public void doAction(String command, String data) {
        switch (command) {
            case "create":
                save(data);
                break;
            case "read":
                findAll();
                break;
            case "update":
                update(data);
                break;
            case "delete":
                delete(data);
                break;
        }
    }

    private void findAll() {
        System.out.println(postsService.findAll());
    }

    private void save(String data){
        postsService.save(data);
    }

    private void update(String data){
        postsService.update(data);
    }

    private void delete(String data){
        postsService.delete(data);
    }
}
