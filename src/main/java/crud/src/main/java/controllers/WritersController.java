package controllers;

import services.WritersService;

public class WritersController {

    private final WritersService writersService;


    public WritersController(WritersService writersService) {
        this.writersService = writersService;
    }

    public void doAction(String command, String data) {
        switch (command) {
            case "create":
                save(data);
                break;
            case "read":
                findById(data);
                break;
            case "update":
                update(data);
                break;
            case "delete":
                delete(data);
                break;
        }
    }

    private void findById(String data){
        writersService.findById(data);
    }

    private void save(String data){
        writersService.save(data);
    }

    private void update(String data){
        writersService.update(data);
    }

    private void delete(String data){
        writersService.delete(data);
    }
}
