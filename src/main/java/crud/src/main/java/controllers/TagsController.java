package controllers;

import repositories.TagRepository;
import services.TagsService;

public class TagsController {

    private final TagsService tagsService;

    public TagsController(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    public void doAction(String command, String data) {
        switch (command) {
            case "create":
                save(data);
                break;
            case "read":
                findAllByPostId(data);
                break;
            case "update":
                update(data);
                break;
            case "delete":
                delete(data);
                break;
        }
    }

    private void findAllByPostId(String data) {
        System.out.println(tagsService.findAllByPostId(data));
    }

    private void save(String data) {
        tagsService.save(data);
    }

    private void update(String data){
        tagsService.update(data);
    }

    private void delete(String data){
        tagsService.delete(data);
    }
}
