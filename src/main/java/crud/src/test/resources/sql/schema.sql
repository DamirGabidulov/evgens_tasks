drop table if exists tag;
drop table if exists post;
drop table if exists writer;

create table writer (
                        id bigint auto_increment primary key,
                        name varchar(30)
);

create table post (
                      id bigint auto_increment primary key,
                      content varchar(100),
                      post_status varchar(30),
                      writer_id bigint,
                      foreign key (writer_id) references writer (id) on delete cascade
);

create table tag (
                     id bigint auto_increment primary key,
                     name varchar(30),
                     post_id bigint,
                     foreign key (post_id) references post (id) on delete cascade
);