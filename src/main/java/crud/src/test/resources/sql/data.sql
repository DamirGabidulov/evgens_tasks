insert into writer (name) values ('Johnny Cash');
insert into writer (name) values ('John Snow');

insert into post (content, post_status, writer_id) values ('news', 'ACTIVE', 1);
insert into post (content, post_status, writer_id) values ('work', 'DELETED', 1);
insert into post (content, post_status, writer_id) values ('money', 'ACTIVE', 2);

insert into tag(name, post_id) values ('breaking', 1);
insert into tag(name, post_id) values ('boring', 2);
insert into tag(name, post_id) values ('fast', 3);
insert into tag(name, post_id) values ('hot', 1);
insert into tag(name, post_id) values ('stock', 3);