package repositories;

import models.Post;
import models.Tag;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class TagRepositoryImplTest {

    private TagRepositoryImpl tagRepository;

    @BeforeEach
    void setUp() {
        DataSource dataSource = new EmbeddedDatabaseBuilder()
                .setType(EmbeddedDatabaseType.H2)
                .addScript("sql\\schema.sql")
                .addScript("sql\\data.sql")
                .build();
        try {
            tagRepository = new TagRepositoryImpl(dataSource.getConnection());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void find_all_tags() {
        List<Tag> expectedTags = new ArrayList<>();
        expectedTags.add(new Tag(1L, "breaking"));
        expectedTags.add(new Tag(2L, "boring"));
        expectedTags.add(new Tag(3L, "fast"));
        expectedTags.add(new Tag(4L, "hot"));
        expectedTags.add(new Tag(5L, "stock"));
        List<Tag> receivedTags = tagRepository.findAll();
        assertEquals(expectedTags, receivedTags);
    }
    
    @ParameterizedTest(name = "return tags with post id = {0}")
    @ValueSource(longs = 3L)
    void find_all_by_post_id(long postId){
        List<Tag> expectedTags = new ArrayList<>();
        expectedTags.add(new Tag(3L, "fast"));
        expectedTags.add(new Tag(5L, "stock"));
        List<Tag> receivedTags = tagRepository.findAllByPostId(postId);
        assertEquals(expectedTags, receivedTags);
    }

    @Test
    void save_tag(){
        Tag tag = Tag.builder()
                .name("new tag")
                .build();
        tagRepository.save(3L, tag);
        Optional<Tag> receivedTag = tagRepository.findAll().stream().filter(t -> t.getName().equals("new tag")).findFirst();
        receivedTag.ifPresent(value -> assertEquals(tag, value));
    }

    @Test
    void update_tag(){
        Optional<Tag> receivedTag = tagRepository.findAll().stream().filter(t -> t.getId().equals(1L)).findFirst();
        String newName = "new name";
        if (receivedTag.isPresent()){
            Tag tag = receivedTag.get();
            tagRepository.updateName(tag.getName(), newName);
            assertEquals(newName, tagRepository.findAll().stream().filter(t -> t.getId().equals(1L)).findFirst().get().getName());
        } else {
            throw new IllegalStateException("Check file with data");
        }
    }

    @ParameterizedTest(name = "return Optional.empty() on tag with id {0}")
    @ValueSource(longs = 1L)
    void delete_tag(long id){
        tagRepository.delete(id);
        assertSame(Optional.empty(), tagRepository.findAll().stream().filter(t -> t.getId().equals(id)).findFirst());
    }
}